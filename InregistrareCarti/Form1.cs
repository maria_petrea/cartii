﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace InregistrareCarti
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void btn1_Click(object sender, EventArgs e)
        {

            Carte c = new Carte();

            c.Titlu = txt1.Text.ToString();
            if (int.TryParse(txt2.Text,out int bbb) )
            {
                MessageBox.Show("Caracterele introduse in campul Autor nu sunt valide. OPERATIUNEA NU SE POATE REALIZA!!!");
                txt2.Text = null;
                return;
            }
            else
            {
                c.Autor = txt2.Text.ToString();
            }
            c.Anul_Publicarii = int.Parse(listBox1.SelectedItem.ToString());
            if (ckb1.Checked == true)
                c.Limba = "Romana";
            if (ckb2.Checked == true)
                c.Limba = "Engleza";
            if (ckb3.Checked == true)
                c.Limba = "Franceza";
            if (!int.TryParse(txtPg.Text, out int dfadf))
            {
                MessageBox.Show("Caracterele introduse in campul Nr pag nu sunt valide. OPERATIUNEA NU SE POATE REALIZA!!!");
                txtPg.Text = null;
                return;
            }
            else
            {
                c.Nr_Pagini = int.Parse(txtPg.Text.ToString());
            }
            if (ckb4.Checked == true)
                c.Domeniu = "SF";
            else
            if (ckb5.Checked == true)
                c.Domeniu = "Arta";
            else
           if (ckb6.Checked == true)
                c.Domeniu = "Beletristica";
            c.Editura = cmbEd.Text;
            if (!double.TryParse(txtPret.Text, out double fasg))
            {
                MessageBox.Show("Caracterele introduse in campul Pret nu sunt valide. OPERATIUNEA NU SE POATE REALIZA!!!");
                txtPret.Text = null;
                return;
            }
            else
            {
                c.Pret = double.Parse(txtPret.Text.ToString());
            }
            MessageBox.Show("A fost adaugata cartea: " + c.Titlu);
            listCarti.Items.Add(c);



            int nr = 0;
            nr = int.Parse(txtCarti.Text);
            nr++;
            txtCarti.Text = nr.ToString();

            txt1.Text = null;
            txt2.Text = null;
            txtPg.Text = null;
            txtPret.Text = null;
            ckb1.Checked = false;
            ckb2.Checked = false; ckb3.Checked = false; ckb4.Checked = false; ckb5.Checked = false; ckb6.Checked = false;
            listBox1.SelectedItem = null;
            cmbEd.SelectedItem = null;

        }

        private void btnSterge_Click_1(object sender, EventArgs e)
        {
            listCarti.Items.Remove(listCarti.SelectedItem);
            int nr = 0;
            nr = int.Parse(txtCarti.Text);
            nr--;
            txtCarti.Text = nr.ToString();
        }
    }
}

    
