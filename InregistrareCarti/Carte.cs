﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InregistrareCarti
{
    class Carte
    {
        public string Titlu;
        public string Autor;
        public int Anul_Publicarii;
        public string Limba;
        public int Nr_Pagini;
        public string Domeniu;
        public string Editura;
        public double Pret;

        public override string ToString()
        {
            return $"  {Titlu}  {","} {Autor} {"-"} {Anul_Publicarii} ";
        }
    }
}
