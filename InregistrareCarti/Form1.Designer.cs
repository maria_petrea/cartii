﻿namespace InregistrareCarti
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.btn1 = new System.Windows.Forms.Button();
            this.lbl7 = new System.Windows.Forms.Label();
            this.txtPg = new System.Windows.Forms.TextBox();
            this.lbl8 = new System.Windows.Forms.Label();
            this.ckb4 = new System.Windows.Forms.CheckBox();
            this.ckb5 = new System.Windows.Forms.CheckBox();
            this.ckb6 = new System.Windows.Forms.CheckBox();
            this.lbl9 = new System.Windows.Forms.Label();
            this.cmbEd = new System.Windows.Forms.ComboBox();
            this.lbl10 = new System.Windows.Forms.Label();
            this.txtPret = new System.Windows.Forms.TextBox();
            this.lbl5 = new System.Windows.Forms.Label();
            this.ckb3 = new System.Windows.Forms.CheckBox();
            this.ckb2 = new System.Windows.Forms.CheckBox();
            this.ckb1 = new System.Windows.Forms.CheckBox();
            this.txtCarti = new System.Windows.Forms.TextBox();
            this.listCarti = new System.Windows.Forms.ListBox();
            this.btnSterge = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(293, 24);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(124, 20);
            this.lbl1.TabIndex = 0;
            this.lbl1.Text = "Inregistrare carti";
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Location = new System.Drawing.Point(76, 89);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(38, 20);
            this.lbl2.TabIndex = 1;
            this.lbl2.Text = "Titlu";
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Location = new System.Drawing.Point(66, 158);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(48, 20);
            this.lbl3.TabIndex = 2;
            this.lbl3.Text = "Autor";
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Location = new System.Drawing.Point(12, 224);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(106, 20);
            this.lbl4.TabIndex = 5;
            this.lbl4.Text = "Anul publicarii";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Items.AddRange(new object[] {
            "1990",
            "2000",
            "2002",
            "2004",
            "2006",
            "2008",
            "2009",
            "2012",
            "2013",
            "2015",
            "2018",
            "2019"});
            this.listBox1.Location = new System.Drawing.Point(141, 224);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 84);
            this.listBox1.TabIndex = 6;
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(141, 83);
            this.txt1.Multiline = true;
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(120, 26);
            this.txt1.TabIndex = 7;
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(141, 152);
            this.txt2.Multiline = true;
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(120, 26);
            this.txt2.TabIndex = 8;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(472, 394);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(85, 42);
            this.btn1.TabIndex = 13;
            this.btn1.Text = "Adauga";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.Location = new System.Drawing.Point(362, 89);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(71, 20);
            this.lbl7.TabIndex = 16;
            this.lbl7.Text = "Nr pagini";
            // 
            // txtPg
            // 
            this.txtPg.Location = new System.Drawing.Point(456, 86);
            this.txtPg.Multiline = true;
            this.txtPg.Name = "txtPg";
            this.txtPg.Size = new System.Drawing.Size(120, 26);
            this.txtPg.TabIndex = 17;
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.Location = new System.Drawing.Point(360, 155);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(73, 20);
            this.lbl8.TabIndex = 18;
            this.lbl8.Text = "Domeniu";
            // 
            // ckb4
            // 
            this.ckb4.AutoSize = true;
            this.ckb4.Location = new System.Drawing.Point(456, 158);
            this.ckb4.Name = "ckb4";
            this.ckb4.Size = new System.Drawing.Size(56, 24);
            this.ckb4.TabIndex = 19;
            this.ckb4.Text = "SF";
            this.ckb4.UseVisualStyleBackColor = true;
            // 
            // ckb5
            // 
            this.ckb5.AutoSize = true;
            this.ckb5.Location = new System.Drawing.Point(456, 188);
            this.ckb5.Name = "ckb5";
            this.ckb5.Size = new System.Drawing.Size(65, 24);
            this.ckb5.TabIndex = 20;
            this.ckb5.Text = "Arta";
            this.ckb5.UseVisualStyleBackColor = true;
            // 
            // ckb6
            // 
            this.ckb6.AutoSize = true;
            this.ckb6.Location = new System.Drawing.Point(456, 218);
            this.ckb6.Name = "ckb6";
            this.ckb6.Size = new System.Drawing.Size(113, 24);
            this.ckb6.TabIndex = 21;
            this.ckb6.Text = "Beletristica";
            this.ckb6.UseVisualStyleBackColor = true;
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.Location = new System.Drawing.Point(362, 268);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(60, 20);
            this.lbl9.TabIndex = 22;
            this.lbl9.Text = "Editura";
            // 
            // cmbEd
            // 
            this.cmbEd.FormattingEnabled = true;
            this.cmbEd.Items.AddRange(new object[] {
            "Polirom",
            "Humanitas",
            "Litera",
            "Arc",
            "Moldova",
            "Cartier",
            "Lumina",
            "Prut International",
            "Stiinta",
            "Teora"});
            this.cmbEd.Location = new System.Drawing.Point(455, 265);
            this.cmbEd.Name = "cmbEd";
            this.cmbEd.Size = new System.Drawing.Size(121, 28);
            this.cmbEd.TabIndex = 23;
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.Location = new System.Drawing.Point(362, 348);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(38, 20);
            this.lbl10.TabIndex = 24;
            this.lbl10.Text = "Pret";
            // 
            // txtPret
            // 
            this.txtPret.Location = new System.Drawing.Point(455, 345);
            this.txtPret.Multiline = true;
            this.txtPret.Name = "txtPret";
            this.txtPret.Size = new System.Drawing.Size(120, 26);
            this.txtPret.TabIndex = 25;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Location = new System.Drawing.Point(66, 344);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(52, 20);
            this.lbl5.TabIndex = 9;
            this.lbl5.Text = "Limba";
            // 
            // ckb3
            // 
            this.ckb3.AutoSize = true;
            this.ckb3.Location = new System.Drawing.Point(141, 404);
            this.ckb3.Name = "ckb3";
            this.ckb3.Size = new System.Drawing.Size(93, 24);
            this.ckb3.TabIndex = 11;
            this.ckb3.Text = "Engleza";
            this.ckb3.UseVisualStyleBackColor = true;
            // 
            // ckb2
            // 
            this.ckb2.AutoSize = true;
            this.ckb2.Location = new System.Drawing.Point(141, 374);
            this.ckb2.Name = "ckb2";
            this.ckb2.Size = new System.Drawing.Size(102, 24);
            this.ckb2.TabIndex = 12;
            this.ckb2.Text = "Franceza";
            this.ckb2.UseVisualStyleBackColor = true;
            // 
            // ckb1
            // 
            this.ckb1.AutoSize = true;
            this.ckb1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ckb1.Location = new System.Drawing.Point(141, 344);
            this.ckb1.Name = "ckb1";
            this.ckb1.Size = new System.Drawing.Size(96, 24);
            this.ckb1.TabIndex = 10;
            this.ckb1.Text = "Romana";
            this.ckb1.UseVisualStyleBackColor = true;
            // 
            // txtCarti
            // 
            this.txtCarti.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCarti.Location = new System.Drawing.Point(895, 24);
            this.txtCarti.Multiline = true;
            this.txtCarti.Name = "txtCarti";
            this.txtCarti.Size = new System.Drawing.Size(73, 45);
            this.txtCarti.TabIndex = 27;
            this.txtCarti.Text = "0";
            // 
            // listCarti
            // 
            this.listCarti.FormattingEnabled = true;
            this.listCarti.ItemHeight = 20;
            this.listCarti.Location = new System.Drawing.Point(601, 101);
            this.listCarti.Name = "listCarti";
            this.listCarti.Size = new System.Drawing.Size(379, 264);
            this.listCarti.TabIndex = 28;
            // 
            // btnSterge
            // 
            this.btnSterge.Location = new System.Drawing.Point(713, 394);
            this.btnSterge.Name = "btnSterge";
            this.btnSterge.Size = new System.Drawing.Size(83, 42);
            this.btnSterge.TabIndex = 29;
            this.btnSterge.Text = "Sterge";
            this.btnSterge.UseVisualStyleBackColor = true;
            this.btnSterge.Click += new System.EventHandler(this.btnSterge_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 613);
            this.Controls.Add(this.btnSterge);
            this.Controls.Add(this.listCarti);
            this.Controls.Add(this.txtCarti);
            this.Controls.Add(this.txtPret);
            this.Controls.Add(this.lbl10);
            this.Controls.Add(this.cmbEd);
            this.Controls.Add(this.lbl9);
            this.Controls.Add(this.ckb6);
            this.Controls.Add(this.ckb5);
            this.Controls.Add(this.ckb4);
            this.Controls.Add(this.lbl8);
            this.Controls.Add(this.txtPg);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.ckb2);
            this.Controls.Add(this.ckb3);
            this.Controls.Add(this.ckb1);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.lbl4);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.lbl2);
            this.Controls.Add(this.lbl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.TextBox txtPg;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.CheckBox ckb4;
        private System.Windows.Forms.CheckBox ckb5;
        private System.Windows.Forms.CheckBox ckb6;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.ComboBox cmbEd;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.TextBox txtPret;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.CheckBox ckb3;
        private System.Windows.Forms.CheckBox ckb2;
        private System.Windows.Forms.CheckBox ckb1;
        private System.Windows.Forms.TextBox txtCarti;
        private System.Windows.Forms.ListBox listCarti;
        private System.Windows.Forms.Button btnSterge;
    }
}

